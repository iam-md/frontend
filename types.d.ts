
type Accred = {
    "id": number,
    "persId": string,
    "unitId": number,
    "statusId": string,
    "classId": string,
    "positionId": 0,
    "duration": string,
    "creator": string,
    "comment": string,
    "origin": string,
    "author": string,
    "Revalman": string,
    "order": number,
    "StartDate":Date,
    "EndDate": Date,
    "ValDate": Date,
    "creationDate": Date,
    "StartValDate": Date,
    "EndValDate": Date
}